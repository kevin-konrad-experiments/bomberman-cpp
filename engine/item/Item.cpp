#include "Item.h"

int Item::getX() const {
    return x;
}

int Item::getY() const {
    return y;
}

char Item::getSymbol() const {
    return symbol;
}

int Item::getBombsRadiusModifier() const {
    return bombsRadiusModifier;
}

int Item::getBombsDelayModifier() const {
    return bombsDelayModifier;
}

int Item::getBombsCountModifier() const {
    return bombsCountModifier;
}

int Item::getActionsCountModifier() const {
    return actionsCountModifier;
}

int Item::getPlayerHealthModifier() const {
    return playerHealthModifier;
}

bool Item::isCanTriggerBombsModifier() const {
    return canTriggerBombsModifier;
}

int Item::getPlayerInvulnerableModifier() const {
    return playerInvulnerableModifier;
}

void Item::setX(int x) {
    Item::x = x;
}

void Item::setY(int y) {
    Item::y = y;
}
