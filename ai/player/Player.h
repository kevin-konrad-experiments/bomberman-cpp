#ifndef BOMBERMAN_PLAYER_H
#define BOMBERMAN_PLAYER_H


#include <string>
#include <vector>

struct Point {
    int row;
    int col;
};

struct TrackObject
{
    Point pt;
    int type;
    int dist;
};

struct ToDo
{
    Point pt;
    char movement;
    int dist;
};

class Player {

private:
    int id;
    int bombDistance;
    int bombDelay;
    int bombCount;
    int actionsPerTurn = 1;
    bool canTriggerBombs = false;
    std::vector<std::string> grid;
    std::vector<std::string> availableActions = { "U", "D", "L", "R", "U", "D", "L", "R", "B", "NOACTION", "T" };

    void initPlayer();
    void initSettings();
    void initTurn(const int& turn);
    void startAction(const int& turn) const;
    void stopAction(const int& turn) const;
    void doRandomActions();
    void receiveGridOrWinner();


    bool isValid(int row, int col);
    int createAIGrid(std::vector<std::vector<int>>& grid_, Point src, Point dest);
    void undoMovement();
    int safeGetBlock(const int row, const int col);
    void checkPath(Point me);
    std::vector<TrackObject> findUsersAndObject();
    std::vector<ToDo> todo;
    void removeUnpraticalPath(std::vector<std::vector<int>>& grid_, Point src_to_go);
    bool doMovement(ToDo first);
    bool isFrontBlockWeak(Point pt);
    bool hasBombed;
    void showMap();
    void panicAlert(Point here_iam);
    bool isValidAndWalkable(int row, int col);
    void justPoopedBombGetOut(Point here_iam);
    bool spidermanBombAvoider(Point here_iam);
    bool canWeKill(Point here_iam);
    std::vector<char> allMovement;
    std::vector<std::vector<int>> ai_grid;
    void MasterMind();
    Point letsLiveAndGoAwayFromBomb(Point here_iam);
    Point here_iam_global;

public:
    Player();
    void play(const int& turn);
    void goUp();
    void goDown();
    void goLeft();
    void goRight();
    void putBomb();
    void doNothing() const;
    void triggerBombs() const;
};


#endif
