#include "Player.h"
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <queue>

const int BOMB_CODE = 97;
const int WEAK_WALL_CODE = 19;
const int WALL_CODE = 99;
const int NO_WAY_CODE = 98;

const char LEFT = 'L';
const char RIGHT = 'R';
const char TOP = 'U';
const char BOTTOM = 'D';

struct queueBlock
{
    Point pt;
    int dist;
};


int rowNum[] = {-1, 0, 0, 1};
int colNum[] = {0, -1, 1, 0};



bool is_digit(std::string str) {
    for (char i : str) {
        if (!isdigit(i)) {
            return false;
        }
    }

    return true;
}


Player::Player() {
    this->initPlayer();
    this->initSettings();
}

void Player::startAction(const int& turn) const {
    std::cout << "START action " << turn << std::endl;
}

void Player::stopAction(const int& turn) const {
    std::cout << "STOP action " << turn << std::endl;
}


void Player::doNothing() const {
    std::cout << "NOACTION" << std::endl;
}

void Player::putBomb() {
    hasBombed = true;
    std::cout << "B" << std::endl;
}

void Player::goUp()  {
    allMovement.push_back(TOP);
    std::cout << "U" << std::endl;
}

void Player::goDown()  {
    allMovement.push_back(BOTTOM);
    std::cout << "D" << std::endl;
}

void Player::goLeft()  {
    allMovement.push_back(LEFT);
    std::cout << "L" << std::endl;
}

void Player::goRight() {
    allMovement.push_back(RIGHT);
    std::cout << "R" << std::endl;
}

void Player::triggerBombs() const {
    std::cout << "T" << std::endl;
}

void expectMessage(const std::string& message) {
    std::string input;
    std::getline(std::cin, input);

    if (input != message) {
        std::cout << "Expected input was '" + message + "', '" + input + "' received";
        throw std::exception();
    }
}

int receivePlayerId() {
    std::string input;
    std::getline(std::cin, input);
    int inputId = std::stoi(input);

    if (std::cin.fail()) {
        throw std::exception();
    }

    return inputId;
}

void checkWinner(const std::string &message) {
    if (message.find("WINNER") == 0) {
        exit(0);
    }
}

void Player::receiveGridOrWinner() {
    std::vector<std::string> grid;
    std::string input;
    std::getline(std::cin, input);

    checkWinner(input);

    std::istringstream buffer(input);
    std::istream_iterator<std::string> begin(buffer);
    std::istream_iterator<std::string> end;
    std::vector<std::string> inputs(begin, end);

    if (!is_digit(inputs[0]) || !is_digit(inputs[0])) {
        std::cout << input;
        return;
    }

    int gridWidth = std::stoi(inputs[0]);
    int gridHeight = std::stoi(inputs[1]);

    for (int i = 0; i < gridHeight; i++) {
        std::string gridLine;
        std::getline(std::cin, gridLine);
        grid.push_back(gridLine);
    }

    this->grid = grid;
}

void Player::initPlayer() {
    expectMessage("START player");
    this->id = receivePlayerId();
    expectMessage("STOP player");
}

void Player::initSettings() {
    expectMessage("START settings");

    std::string inputSetting;
    do {
        std::getline(std::cin, inputSetting);
        // TODO : use setting
    } while(inputSetting != "STOP settings");
}

void Player::initTurn(const int& turn) {
    expectMessage("START turn " + std::to_string(turn));
    this->receiveGridOrWinner();
    expectMessage("STOP turn " + std::to_string(turn));
}

void Player::play(const int& turn) {
    this->initTurn(turn);
    this->startAction(turn);

    for (int i = 0; i < actionsPerTurn; i++){
        this->doRandomActions();
    }

    this->stopAction(turn);
}


void Player::showMap(){
    std::cout << std::endl;
    for(std::vector<int>::size_type i = 0; i != ai_grid.size(); i++) {
        std::cout << std::endl;
        for(std::vector<int>::size_type j = 0; j != ai_grid[i].size(); j++) {
            std::cout << "-" << ai_grid[i][j];
        }
    }
    std::cout << std::endl;
}

void Player::doRandomActions() {
    std::random_device rd;
    std::default_random_engine engine(rd());
    std::uniform_int_distribution<long> uniform_dist(0, availableActions.size());

    //if(id == 1){
        MasterMind();
    //}

}

void Player::MasterMind(){

    std::vector<TrackObject> findAll = findUsersAndObject();
    here_iam_global = findAll[0].pt;


    TrackObject toTrack = findAll[0];

    for (int i = 0; i<findAll.size(); i++) {
        if(i == 0){
            // std::cout << "me";
        } else {
            Point src_to_go = findAll[i].pt;
            int mini = createAIGrid(ai_grid, src_to_go, here_iam_global);

            if((mini < toTrack.dist && mini >= 0) || toTrack.dist == -1){
                if (mini==2 && toTrack.type == 0 ){
                    // std::cout << "same IA react same so fix loop *disable if fight with others*";
                }
                else{
                    toTrack = findAll[i];
                    toTrack.dist = mini;
                }
            }
        }
    }

    //std::cout << "dist " << toTrack.dist << " type " << toTrack.type << " " << toTrack.pt.col << " " << toTrack.pt.row << std::endl;


    int mini = createAIGrid(ai_grid, toTrack.pt, here_iam_global);

        //showMap();

        if(hasBombed){
            panicAlert(here_iam_global);
            hasBombed = false;
            return;
        }

    if(spidermanBombAvoider(here_iam_global)){
        Point e = letsLiveAndGoAwayFromBomb(here_iam_global);
        //std::cout << "row  " << e.row << " col " << e.col << std::endl;
        mini = createAIGrid(ai_grid, e, here_iam_global);
    }

        if(mini >= 0){

            checkPath(here_iam_global);

            //BombAlert
           if (canWeKill(here_iam_global)){
                putBomb();
            }

            if ((int)todo.size() > 0) {

                ToDo first = todo.front();

                //std::cout << first.movement << " distance " << first.dist << std::endl;
                //std::cout << " row " << first.pt.row << " col " << first.pt.col << std::endl;

                //Is Safe
                if(doMovement(first)){
                    todo.erase(todo.begin());
                }
            }
        }
}

Point Player::letsLiveAndGoAwayFromBomb(Point here_iam){


    bool visited[ai_grid.size()][ai_grid.front().size()];
    memset(visited, false, sizeof visited);

    visited[here_iam.row][here_iam.col] = true;

    std::queue<queueBlock> q;

    queueBlock s = {here_iam, 0};
    q.push(s);

    while (!q.empty())
    {

        queueBlock curr = q.front();
        Point pt = curr.pt;

        q.pop();

        for (int i = 0; i < 4; i++)
        {
            int row = pt.row + rowNum[i];
            int col = pt.col + colNum[i];

            if (isValidAndWalkable(row, col) && !visited[row][col] && ai_grid[row][col] != 0  ) {

                visited[row][col] = true;
                queueBlock Adjcell = { {row, col},
                                       curr.dist + 1 };
                q.push(Adjcell);

                if(!spidermanBombAvoider({row, col})){
                    return {row, col};
                }
            }
        }
    }

    return here_iam;

}

bool Player::canWeKill(Point here_iam){

    int loopBlocks = 3;

    for (int i = 0; i<4; i++) {
        int row = here_iam.row;
        int col = here_iam.col;

        for (int j = 0; j<loopBlocks; j++) {
            row += rowNum[i];
            col += colNum[i];

            if (isValid(row, col)) {
                char block = grid[row][col];

                if (block == WALL_CODE || block == WEAK_WALL_CODE) {
                    break;
                }
                else if (isdigit(block)){
                    return true;
                }
            }
            else {
                break;
            }
        }
    }

    return false;
}

bool Player::spidermanBombAvoider(Point here_iam){

    int loopBlocks = (int)std::max(ai_grid.size(), ai_grid.front().size());

    for (int i = 0; i<4; i++) {
        int row = here_iam.row;
        int col = here_iam.col;

        for (int j = 0; j<loopBlocks; j++) {
            row += rowNum[i];
            col += colNum[i];

            if (isValid(row, col)) {
                int block = ai_grid[row][col];
                if (block == WALL_CODE || block == WEAK_WALL_CODE) {
                    break;
                }
                else if (block == BOMB_CODE){
                    return true;
                }
            }
            else {
                break;
            }
        }
    }

    return false;
}

void Player::panicAlert(Point here_iam){
   /* if (allMovement.size() > 0 ) {
        undoMovement();
        return;
    }*/

    justPoopedBombGetOut(here_iam);
}

void Player::justPoopedBombGetOut(Point here_iam){

    for (int i = 0; i < 4; i++)
    {
        int row = here_iam.row + rowNum[i];
        int col = here_iam.col + colNum[i];

        if( isValidAndWalkable(row, col)){
            switch (i) {
                case 0:
                    goUp();
                    return;
                case 1:
                    goLeft();
                    return;
                case 2:
                    goRight();
                    return;
                case 4:
                    goDown();
                    return;
                default:
                    doNothing();
                    return;
            }
        }
    }
}


bool Player::isValidAndWalkable(int row, int col){

    if(!isValid(row, col)){
        return false;
    }
    char block = grid[row][col];
    if( block == '#' || block == '%' || block == 'o' || isdigit(block)) {
        return false;
    }

    return true;
}

bool Player::isFrontBlockWeak(Point pt){
    char block = grid[pt.row][pt.col];
    if(block == '%'){
        return true;
    }
    return false;
}

bool Player::doMovement(ToDo first){

    if(spidermanBombAvoider(first.pt)){
        doNothing();
        return false;
    }
    if (isFrontBlockWeak(first.pt)) {
        putBomb();
        return false;
    }
    if(isValidAndWalkable(first.pt.row, first.pt.col)){
        switch (first.movement) {
            case LEFT:
                goLeft();
                return true;
            case RIGHT:
                goRight();
                return true;
            case BOTTOM:
                goDown();
                return true;
            case TOP:
                goUp();
                return true;
            default:
                doNothing();
                return false;
        }
    } else{
        justPoopedBombGetOut(here_iam_global);
        return false;
    }


}

bool Player::isValid(int row, int col){
    return (row >= 0) && (row < ai_grid.size()) &&
           (col >= 0) && (col < ai_grid.front().size());
}

void Player::removeUnpraticalPath(std::vector<std::vector<int>>& grid_, Point src_to_go) {

    for(int i = 0; i < grid_.size(); i++ ){
        for(int j = 0; j < grid_[i].size(); j++){
            if (grid[i][j] == 'o'){
                grid_[i][j] = BOMB_CODE;
            }
            else if ( grid_[i][j] == 1 ){
                grid_[i][j] = NO_WAY_CODE;
            } else if (grid_[i][j] == 0){
                grid_[i][j] = WALL_CODE;
            }
        }
    }

    grid_[src_to_go.row][src_to_go.col] = 1;
}

int Player::createAIGrid(std::vector<std::vector<int>>& grid_, Point src_to_go, Point here_iam) {

    grid_ = {};

    for(int i = 0; i < grid.size(); i++ ){
        std::vector<int> line;
        for(int j = 0; j < grid[i].size(); j++){
            char block = grid[i][j];
            if ( block == '#' || block == 'o' ){
                line.push_back(0);
            } else {
                line.push_back(1);
            }
        }
        ai_grid.push_back(line);
    }

    if (grid_[src_to_go.row][src_to_go.col] == 0 || grid_[here_iam.row][here_iam.col] == 0)
        return -2;

    bool visited[grid_.size()][grid_.front().size()];
    memset(visited, false, sizeof visited);

    visited[src_to_go.row][src_to_go.col] = true;

    std::queue<queueBlock> q;

    queueBlock s = {src_to_go, 0};
    q.push(s);

    while (!q.empty())
    {

        queueBlock curr = q.front();
        Point pt = curr.pt;

        if (pt.row == here_iam.row && pt.col == here_iam.col) {
            removeUnpraticalPath(grid_, src_to_go);
            return curr.dist;
        }

        q.pop();

        for (int i = 0; i < 4; i++)
        {
            int row = pt.row + rowNum[i];
            int col = pt.col + colNum[i];

            if (isValid(row, col) && !visited[row][col] && grid_[row][col] != 0  ) {

                visited[row][col] = true;
                queueBlock Adjcell = { {row, col},
                                       curr.dist + 1 };
                q.push(Adjcell);

                if ( ai_grid[row][col] == 1) {
                    int ei = ai_grid[pt.row][pt.col] + 1;
                    ai_grid[row][col] = ei;
                }
            }
        }
    }

    return -1;
}

void Player::undoMovement(){
    char movement = allMovement.back();

    switch (movement) {
        case LEFT:
            goRight();
            break;
        case RIGHT:
            goLeft();
            break;
        case BOTTOM:
            goUp();
            break;
        case TOP:
            goDown();
            break;
        default:
            goLeft();
            break;
    }

    allMovement.pop_back();
}

int Player::safeGetBlock(const int row, const int col) {

    int safe_row = row < ai_grid.size() ? row : (int)ai_grid.size() - 1;
    int safe_col = col < ai_grid[safe_row].size() ? col : (int)ai_grid[safe_row].size() - 1;

    return ai_grid[safe_row][safe_col];
}

void Player::checkPath(Point me){

    bool notFound = true;
    todo = {};

    while (notFound) {

        //std::cout << std::endl;

        if ( safeGetBlock(me.row, me.col) == 1 ){
            notFound = false;
            putBomb();
            return;
        }


        if ( safeGetBlock(me.row, me.col + 1) < safeGetBlock(me.row, me.col) ) {
            me.col = me.col + 1;
            todo.push_back({me, RIGHT, safeGetBlock(me.row, me.col)});
            //doMovement(RIGHT, me);
        }

        else if ( safeGetBlock(me.row, me.col - 1) < safeGetBlock(me.row, me.col) )  {
            me.col = me.col - 1;
            todo.push_back({me, LEFT, safeGetBlock(me.row, me.col)});
            //doMovement(LEFT, me);
        }


        else if ( safeGetBlock(me.row + 1, me.col) < safeGetBlock(me.row, me.col) )  {
            me.row = me.row + 1;
            todo.push_back({me, BOTTOM, safeGetBlock(me.row, me.col)});
            //doMovement(BOTTOM, me);
        }


        else if ( safeGetBlock(me.row - 1, me.col) < safeGetBlock(me.row, me.col) )  {
            me.row = me.row - 1;
            todo.push_back({me, TOP, safeGetBlock(me.row, me.col)});
            //doMovement(TOP, me);
        }

        else {
            ai_grid[me.row][me.col] = 999;
            me = todo.back().pt;
            todo.pop_back();
            putBomb();
        }

        notFound = false;
        //showMap();
    }
}

std::vector<TrackObject> Player::findUsersAndObject() {

    std::vector<TrackObject> objectsPostion;

    for(int i = 0; i < grid.size(); i++) {
        for(int j = 0; j < grid[i].size(); j++) {
            char block = grid[i][j];
            char my_id = (char)('0' + id);

            Point position;
            if (my_id == block) {
                position.col = (int)j;
                position.row = (int)i;
                objectsPostion.insert(objectsPostion.begin(), {position,0, -1});
            }
            else if( isdigit(block) && block != my_id) {
                position.col = (int)j;
                position.row = (int)i;
                objectsPostion.push_back({position,0, -1});
            } else if ( block == 'N' ||
                        block == 'A' ||
                        block == 'a' ||
                        block == 'D' ||
                        block == 'n' ||
                        block == 'H' ||
                        block == 'd' ||
                        block == 'G' ||
                        block == 'R' ) {
                position.col = (int)j;
                position.row = (int)i;
                objectsPostion.push_back({position,1,-1});
            }
        }
    }

    return objectsPostion;
}